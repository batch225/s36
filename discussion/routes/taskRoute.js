// ! Contains all the endpoints for our application

// We need to use express' Router() function to achieve this.

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middleware that makes it easier to create routes for our application.
const router = express.Router();

const taskController = require("../controllers/taskController");

// ! Routes:

// * Route to get all the tasks
router.get("/", (req, res) => {

  taskController.getAllTasks().then((result) => res.send(result));

});

// * Route to create a new task
router.post("/create", (req, res) => {
  
  // If information will be coming from client side commonly from forms, the data can be accessed from the request "body" property
  taskController.createTasks(req.body).then(result => res.send(result));
});

// Use "module.exports" to export the router object to use in the "app.js"

module.exports = router;

// * Route to DELETE a task
// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
/*
  Example:
  localhost:3000/tasks/delete/1234
  The 1234 is assigned to the "id" parameter in the URL
*/

router.delete("/delete/:id", (req, res) => {

  // URL parameter values are accessed via the request object's "params" property
  // The property name of this object will match the given URL parameter name
  // In this case "id" is the name of the parameter
  // If information will be coming from the URL, the data can be accessed from the request "params" property
  taskController.deleteTasks(req.params.id).then(result => res.send(result));
});

// * Route to UPDATE a task
router.put("/update/:id", (req,res) => {

  taskController.updateTasks(req.params.id, req.body).then(result => res.send(result));
});

// ? : In this code, the getAllTasks() and createTasks() methods are called and they return a promise. The .then() method is called on the returned promise, and it takes a callback function as an argument. This callback function is executed when the promise is resolved (i.e. when the asynchronous operation is completed successfully).

// ? : The callback function takes the resolved value (in this case, resultFromController) as an argument, and it is used to send the result as a response to the client.

// ? : So, the .then() method is used to handle the promise returned by the getAllTasks() and createTasks() method, and it makes sure that the response to the client is sent only after the promise is resolved.

// ACTIVITY CODES: 

router.get("/:id", (req, res) => {

  taskController.getTask(req.params.id).then((result) => res.send(result));

});

router.put("/:id/complete", (req,res) => {

  taskController.completeTask(req.params.id).then(result => res.send(result));
});