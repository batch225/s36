// Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();

const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connect to MongoDB Atlas

mongoose.connect(`mongodb+srv://ronb077:${process.env.PASSWORD}@cluster0.umwequr.mongodb.net/s36_MRC?retryWrites=true&w=majority`, {

     // In simple words, "useNewUrlParser : true" allows us to avoid any current and future errors while connecting to MongoDB
    useNewUrlParser : true,

    // False by default. Set to true opt in to using MongoDB driver's new connection management engine. You shoult set this option to true, except for the unlikely case that it prevents you from mainting a stable connection
    useUnifiedTopology : true
});

// Setup notification for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB"));

// Add the task routes
// Allows all the tasks routes created in the "taskRoutes.js" file to use " " route

app.use("/tasks", taskRoute);


// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));



