// Create the Schema, model and export file

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

// "module.exports" is the way for Node JS to treat this value as "package" that can be used by other files.

module.exports = mongoose.model("Task", taskSchema);

// ? : This code defines a Mongoose schema for a task object and exports the schema as a Mongoose model.

// ? : The first line imports the Mongoose library, which allows for interaction with MongoDB databases.

// ? : The taskSchema constant is then defined as a new Mongoose schema. A Mongoose schema defines the structure of the documents that will be stored in a MongoDB collection.

// ? : The taskSchema has two properties, name and status. name property is of type String and the status property is an object with type of string and a default value of "pending"

// ? : The last line exports the schema as a Mongoose model, named "Task", which can be used to perform CRUD (CREATE, READ, UPDATE & DELETE) operations on the tasks collection in the MongoDB database.

// ? : The exported model can be used to create, read, update, and delete tasks in the MongoDB collection.



