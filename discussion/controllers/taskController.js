// Controllers contain the functions and business logic of our Express JS. All the operation it can do will be placed in this file

const { findByIdAndUpdate } = require("../models/task");
const Task = require("../models/task");

module.exports.getAllTasks = () => {
    
    // The "return" statement returns the result of the Mongoose method
    // The "then" method is used to wait for the Mongoose method to finish before sending a result.\ back to the route.

    return Task.find({}).then(result => {
        return result;
    })

};

module.exports.createTasks = (requestBody) => {

    // Create a task object based on the Mongoose model: "Task"

    let newTask = new Task({
        name: requestBody.name
    })

    return newTask.save().then((task, error) => {
        if (error) {
            console.log(error);
        
        // If an error is encountered, the "return" statement will prevent any other line or codewithin the same code block
        // The else statement will no longer be evaluated
        return false;

        } else {

        return task
        
        }
    })

     
};

module.exports.deleteTasks = (taskId) => {

    // The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
    return Task.findByIdAndRemove(taskId).then((result, err) => {

        if (err) {
             console.log(error);
             return false
        } else {
            return result
        }
    })

};

// module.exports.updateTasks = (taskId, newContent) => {

//     return Task.findById(taskId).then((result, err) => {
//         if (err) {
//             console.log(err);
//             return false
//         } else {
            
//         result.name = newContent.name;

//         return result.save().then((updatedTask, saveErr) => {
//             if (saveErr) {

//                 console.log(saveErr);
//                 return false
//             } else {
//                 return updatedTask
//             }
//         })
//         }       
//     })
// };


// TODO: More efficient code using the findByIdAndUpdate method

module.exports.updateTasks = (taskId, newContent) => {
    return Task.findByIdAndUpdate(taskId, newContent, {new: true}).then((updatedTask, err) => {
        if (err) {
            console.log(err);
            return false
        } else {
            return updatedTask
        }       
    })
};

// TODO : The findByIdAndUpdate() method takes the taskId and newContent as its arguments, and the new: true option is passed to return the updated document instead of the original document.

// TODO : The rest of the code can remain the same, as the findByIdAndUpdate() method already handles saving the updated task to the database.

// TODO : Also, you don't need to check for the saveErr as the findByIdAndUpdate() method returns a promise that rejects with an error if the update operation fails.


// ? : This is Node.js code that exports two functions. The first function, getAllTasks, is a function that retrieves all tasks from the database using the Task model. The Task.find({}) method is used to retrieve all tasks from the collection. The result is then returned as a promise.

// ? : The second function, createTasks, is a function that creates a new task in the database using the Task model. It takes in a request body as its parameter, which is used to create a new task object. The name property of the task object is set to the name property of the request body. The save() method is then used to save the new task object to the database. If there is an error, it logs the error and returns false, otherwise, it returns the task object.

// ACTIVITY CODES: 

module.exports.getTask = (taskId) => {

    return Task.findById(taskId).then(result => {
        return result;
    })

};

module.exports.completeTask = (taskId) => {
    let newStatus = {
        status: "completed"
    }
    return Task.findByIdAndUpdate(taskId, newStatus, {new: true}).then((newStatus, err) => {
        if (err) {
            console.log(err);
            return false
        } else {
            return newStatus
        }       
    })
};


